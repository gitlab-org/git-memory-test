# Test loading large diffs with gitlab_git

This is to test the version of gitlab_git checked out in `../gitlab_git`. Print
the RSS, load a large diff, and print the RSS again.

```
bundle exec ruby load-large-diff.rb
```

Example output:

```
$ bundle exec ruby load-large-diff.rb 
 62512
1375184
```

Here we see the memory use climb from 61MB to 1342MB.
